package cn.uncode.baas.server.dto;

import java.io.Serializable;

public class RestField implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8539801603569278105L;
	
	public static final String ID = "id";
	public static final String BUCKET = "bucket";
	public static final String METHOD_ID = "methodId";
	public static final String FIELD_NAME = "fieldName";
	public static final String REQUEST = "request";
	public static final String REGULAR = "regular";
	public static final String STATUS = "status";
	
	private int id;
	private String bucket;
	private int methodId;
	private String fieldName;
	private boolean request;
	private String regular;
	private int status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMethodId() {
		return methodId;
	}
	public void setMethodId(int methodId) {
		this.methodId = methodId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public boolean isRequest() {
		return request;
	}
	public void setRequest(boolean request) {
		this.request = request;
	}
	public String getRegular() {
		return regular;
	}
	public void setRegular(String regular) {
		this.regular = regular;
	}
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	

}
